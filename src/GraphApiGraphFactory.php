<?php

namespace Drupal\ms_graph_api;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigValueException;
use Drupal\key\Entity\Key;
use Drupal\key\KeyInterface;
use Drupal\key\KeyRepositoryInterface;
use Drupal\ms_graph_api\Exception\AccessTokenParseException;
use Drupal\ms_graph_api\Exception\AccessTokenRequestException;
use Drupal\ms_graph_api\Plugin\KeyType\GraphApiKeyType;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\UriTemplate\UriTemplate;
use Microsoft\Graph\Graph;

/**
 * The default MS Graph API graph factory.
 *
 * @noinspection PhpUnused
 */
class GraphApiGraphFactory implements GraphApiGraphFactoryInterface {

  /**
   * The URI of the Microsoft OAuth token issuer endpoint.
   */
  const MS_TOKEN_ENDPOINT_URI =
    'https://login.microsoftonline.com/{tenant_id}/oauth2/token?api-version=1.0';

  /**
   * The URI of the graph resource to request access to.
   */
  const MS_GRAPH_RESOURCE = 'https://graph.microsoft.com/';

  /**
   * The factory for obtaining configuration settings.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The repository of secure keys.
   *
   * @var \Drupal\key\KeyRepositoryInterface
   */
  protected $keyRepository;

  /**
   * Constructor for GraphApiGraphFactory.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for obtaining configuration settings.
   * @param \Drupal\key\KeyRepositoryInterface $key_repository
   *   The repository of secure keys.
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                              KeyRepositoryInterface $key_repository) {
    $this->configFactory = $config_factory;
    $this->keyRepository = $key_repository;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultTenantDomain(): string {
    $default_key_id = $this->getDefaultKeyId();

    return $this->getTenantDomainFromKeyId($default_key_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getTenantDomainFromKeyId(string $key_id): string {
    $key = $this->getKey($key_id);

    return $this->getTenantDomainFromKey($key);
  }

  /**
   * {@inheritdoc}
   */
  public function getTenantDomainFromKey(KeyInterface $key): string {
    $key_type = $key->getKeyType();
    $key_id   = $key->id();

    $this->checkKeyType($key);
    assert($key_type instanceof GraphApiKeyType);

    $key_config    = $key_type->unserialize($key->getKeyValue());
    $tenant_domain = $key_config[Constants::KEY_VALUE_TENANT_DOMAIN] ?? NULL;

    if (empty($tenant_domain)) {
      throw new ConfigValueException(
        sprintf(
          "There is no 'Tenant Domain' value defined in the provided MS Graph API key (%s).",
          $key_id
        )
      );
    }

    return $tenant_domain;
  }

  /**
   * {@inheritdoc}
   */
  public function buildDefaultGraph(): Graph {
    $default_key_id = $this->getDefaultKeyId();

    return $this->buildGraphFromKeyId($default_key_id);
  }

  /**
   * {@inheritdoc}
   */
  public function buildGraphFromKeyId(string $key_id): Graph {
    $key = $this->getKey($key_id);

    return $this->buildGraphFromKey($key);
  }

  /**
   * {@inheritdoc}
   */
  public function buildGraphFromKey(KeyInterface $key): Graph {
    $key_type = $key->getKeyType();
    $key_id   = $key->id();

    $this->checkKeyType($key);
    assert($key_type instanceof GraphApiKeyType);

    $key_config = $key_type->unserialize($key->getKeyValue());

    $tenant_id     = $key_config[Constants::KEY_VALUE_TENANT_ID] ?? NULL;
    $client_id     = $key_config[Constants::KEY_VALUE_CLIENT_ID] ?? NULL;
    $client_secret = $key_config[Constants::KEY_VALUE_CLIENT_SECRET] ?? NULL;

    if (empty($tenant_id)) {
      throw new ConfigValueException(
        sprintf(
          "There is no 'Tenant ID' value defined in the provided MS Graph API key (%s).",
          $key_id
        )
      );
    }

    if (empty($client_id)) {
      throw new ConfigValueException(
        sprintf(
          "There is no 'Client ID' value defined in the provided MS Graph API key (%s).",
          $key_id
        )
      );
    }

    if (empty($client_secret)) {
      throw new ConfigValueException(
        sprintf(
          "There is no 'Client secret' value defined in the provided MS Graph API key (%s).",
          $key_id
        )
      );
    }

    $access_token =
      $this->obtainAccessToken($tenant_id, $client_id, $client_secret);

    $graph = new Graph();
    $graph->setAccessToken($access_token);

    return $graph;
  }

  /**
   * Gets the configuration factory.
   *
   * @return \Drupal\Core\Config\ConfigFactoryInterface
   *   The factory for obtaining configuration settings.
   */
  protected function getConfigFactory(): ConfigFactoryInterface {
    return $this->configFactory;
  }

  /**
   * Gets the key repository.
   *
   * @return \Drupal\key\KeyRepositoryInterface
   *   The repository of secure keys.
   */
  protected function getKeyRepository(): KeyRepositoryInterface {
    return $this->keyRepository;
  }

  /**
   * Gets the ID of the key that contains the system-wide default API key.
   *
   * @return string
   *   The ID of the default key to use for MS Graph API clients.
   */
  protected function getDefaultKeyId(): string {
    $config_id     = Constants::MODULE_CONFIG_ID;
    $config_key_id = Constants::CONFIG_KEY_DEFAULT_KEY_ID;

    $config         = $this->getConfigFactory()->get($config_id);
    $default_key_id = $config->get($config_key_id) ?? NULL;

    if (empty($default_key_id)) {
      throw new ConfigValueException(
        sprintf(
          'A default key for MS Graph (%s.%s) has not been configured.',
          $config_id,
          $config_key_id
        )
      );
    }

    return $default_key_id;
  }

  /**
   * Gets the MS Graph API key that has the specified ID.
   *
   * @return \Drupal\key\Entity\Key
   *   The pre-shared key.
   */
  protected function getKey($key_id): Key {
    $key = $this->getKeyRepository()->getKey($key_id);

    if (empty($key)) {
      throw new \InvalidArgumentException(
        sprintf(
          "Could not locate the specified key ('%s'). Please specify a different key.",
          $key_id
        )
      );
    }

    return $key;
  }

  /**
   * Authenticates with Microsoft Graph to obtain an access token.
   *
   * @param string $tenant_id
   *   The UUID of the tenant.
   * @param string $client_id
   *   The UUID that identifies the application that is authenticating.
   * @param string $client_secret
   *   The secret key for the client.
   *
   * @throws \Drupal\ms_graph_api\Exception\AccessTokenRequestException
   *   If the connection to Microsoft's authentication servers cannot be
   *   established, redirects indefinitely, or the client ID or other
   *   authentication parameters are invalid.
   * @throws \Drupal\ms_graph_api\Exception\AccessTokenParseException
   *   If the request for an access token was successful but resulted in no
   *   usable access token.
   */
  protected function obtainAccessToken(string $tenant_id,
                                       string $client_id,
                                       string $client_secret) {
    $endpoint_uri =
      UriTemplate::expand(
        self::MS_TOKEN_ENDPOINT_URI,
        ['tenant_id' => $tenant_id]
      );

    $http_client = new Client();

    $request_options = [
      'form_params' => [
        'client_id'     => $client_id,
        'client_secret' => $client_secret,
        'resource'      => self::MS_GRAPH_RESOURCE,
        'grant_type'    => 'client_credentials',
      ],
    ];

    try {
      $response = $http_client->post($endpoint_uri, $request_options);
    }
    catch (RequestException $ex) {
      throw new AccessTokenRequestException(
        'Failed to request access token: ' . $ex->getMessage(),
        0,
        $ex
      );
    }

    $tokens_json = $response->getBody()->getContents();

    if (empty($tokens_json)) {
      throw new AccessTokenParseException(
        'An empty access token payload was returned.'
      );
    }

    $tokens_decoded = json_decode($tokens_json);

    return $tokens_decoded->access_token;
  }

  /**
   * Ensures the given key is a compatible type for MS Graph API.
   *
   * @param \Drupal\key\Entity\Key $key
   *   The key to validate.
   */
  protected function checkKeyType(Key $key): void {
    $key_type = $key->getKeyType();
    $key_id   = $key->id();

    if (!($key_type instanceof GraphApiKeyType)) {
      throw new ConfigValueException(
        sprintf(
          "The provided key (%s) does not contain MS Graph API credentials. An 'ms_graph_api' key type was expected.",
          $key_id
        )
      );
    }
  }

}
