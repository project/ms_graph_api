<?php

namespace Drupal\ms_graph_api;

/**
 * A class containing all constants used in multiple places of this module.
 *
 * Where possible, constants specific to an interface or specific classes should
 * live in those interfaces and classes instead of this class.
 */
abstract class Constants {

  /**
   * The machine name for this module.
   */
  const MODULE_NAME = 'ms_graph_api';

  /**
   * The machine name of the config for this module.
   */
  const MODULE_CONFIG_ID = 'ms_graph_api.settings';

  /**
   * The name of the key in the module config holding the PSK key ID.
   */
  const CONFIG_KEY_DEFAULT_KEY_ID = 'default_key_id';

  /**
   * The name of the value in Graph API key types containing the tenant domain.
   *
   * This is the primary domain of the tenant.
   */
  const KEY_VALUE_TENANT_DOMAIN = 'tenant_domain';

  /**
   * The name of the value in Graph API key types containing the tenant ID.
   *
   * This is a UUID.
   */
  const KEY_VALUE_TENANT_ID = 'tenant_id';

  /**
   * The name of the value in Graph API key types containing the client ID.
   *
   * This is a UUID.
   */
  const KEY_VALUE_CLIENT_ID = 'client_id';

  /**
   * The name of the value in Graph API key types containing the client secret.
   */
  const KEY_VALUE_CLIENT_SECRET = 'client_secret';

}
