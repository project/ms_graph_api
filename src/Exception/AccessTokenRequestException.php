<?php

namespace Drupal\ms_graph_api\Exception;

/**
 * Exception thrown if the request for the MS Graph access token fails.
 */
class AccessTokenRequestException extends \RuntimeException {
}
