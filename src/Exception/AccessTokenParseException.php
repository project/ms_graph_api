<?php

namespace Drupal\ms_graph_api\Exception;

/**
 * Exception thrown if the MS Graph access token is empty or un-parseable.
 */
class AccessTokenParseException extends \RuntimeException {
}
