<?php

namespace Drupal\ms_graph_api\Plugin\KeyType;

use Drupal\key\Plugin\KeyType\AuthenticationMultivalueKeyType;

/**
 * Defines a key that stores Graph API connection information.
 *
 * The underlying key is serialized into JSON format.
 *
 * @KeyType(
 *   id = "ms_graph_api",
 *   label = @Translation("MS Graph API Key"),
 *   description = @Translation("Stores a public and private key required to authenticate with Microsoft Graph API."),
 *   group = "connection_string",
 *   key_value = {
 *     "plugin" = "ms_graph_api"
 *   },
 *   multivalue = {
 *     "enabled" = true,
 *     "fields" = {
 *       "tenant_id" = {
 *         "label" = @Translation("Tenant ID"),
 *         "required" = true
 *       },
 *       "client_id" = {
 *         "label" = @Translation("Client ID"),
 *         "required" = true
 *       },
 *       "client_secret" = {
 *         "label" = @Translation("Client secret"),
 *         "required" = true
 *       },
 *     }
 *   }
 * )
 */
class GraphApiKeyType extends AuthenticationMultivalueKeyType {

}
