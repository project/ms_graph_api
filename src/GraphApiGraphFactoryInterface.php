<?php

namespace Drupal\ms_graph_api;

use Drupal\key\KeyInterface;
use Microsoft\Graph\Graph;

/**
 * An interface for obtaining API graphs that can communicate with Microsoft.
 */
interface GraphApiGraphFactoryInterface {

  /**
   * Gets the tenant domain from the system-default key.
   *
   * A default API key must have been configured on the site.
   *
   * @return string
   *   The default tenant domain.
   *
   * @throws \Drupal\Core\Config\ConfigValueException
   *   If either a default API key is not configured or is not the correct type
   *   of key.
   */
  public function getDefaultTenantDomain(): string;

  /**
   * Gets the tenant domain from the the Key with the specified ID.
   *
   * @param string $key_id
   *   The ID of the key containing Graph API credentials.
   *
   * @return string
   *   The tenant domain specified in the key.
   *
   * @throws \Drupal\Core\Config\ConfigValueException
   *   If either the specified API key does not exist or it is not the correct
   *   type of key.
   */
  public function getTenantDomainFromKeyId(string $key_id): string;

  /**
   * Gets the tenant domain from the specified Key.
   *
   * @param \Drupal\key\KeyInterface $key
   *   The key containing Graph API credentials.
   *
   * @return string
   *   The tenant domain specified in the key.
   *
   * @throws \Drupal\Core\Config\ConfigValueException
   *   If the provided key is not the correct type of key.
   */
  public function getTenantDomainFromKey(KeyInterface $key): string;

  /**
   * Builds a MS Graph API graph instance using a system-default key.
   *
   * A default API key must have been configured on the site.
   *
   * @return \Microsoft\Graph\Graph
   *   The default graph instance.
   *
   * @throws \Drupal\Core\Config\ConfigValueException
   *   If either a default API key is not configured or is not the correct type
   *   of key.
   */
  public function buildDefaultGraph(): Graph;

  /**
   * Builds a MS Graph API graph using the Key with the specified ID.
   *
   * @param string $key_id
   *   The ID of the key containing Graph API credentials.
   *
   * @return \Microsoft\Graph\Graph
   *   The graph instance.
   *
   * @throws \Drupal\Core\Config\ConfigValueException
   *   If either the specified API key does not exist or it is not the correct
   *   type of key.
   */
  public function buildGraphFromKeyId(string $key_id): Graph;

  /**
   * Builds a MS Graph API graph using the specified Key.
   *
   * @param \Drupal\key\KeyInterface $key
   *   The key containing Graph API credentials.
   *
   * @return \Microsoft\Graph\Graph
   *   The default graph instance.
   *
   * @throws \Drupal\Core\Config\ConfigValueException
   *   If the provided key is not the correct type of key.
   */
  public function buildGraphFromKey(KeyInterface $key): Graph;

}
