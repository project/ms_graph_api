# MS Graph API
A lightweight wrapper around the
[Microsoft Graph SDK for PHP](https://github.com/microsoftgraph/msgraph-sdk-php),
pulled-in via Composer, for Drupal 8.x.

## Use Case
This module is intended to provide an API for other modules to easily obtain
either a site-wide "default" instance of the MS Graph API client using service
injection, or to be able to instantiate an API client using any number of
custom keys.

This module does not provide site builders or end users with any additional
functionality. Install it only if you are developing a custom module or are
using a module that indicates it depends on this module.

## Usage
### Installation
It is recommended to install this via Composer so that the required version of
the Graph API PHP SDK can be pulled in.

```sh
composer require 'drupal/ms_graph_api'
```

### Registering your Drupal Site as an Application in Azure AD
In order to use this module you will need to create an "Application
registration" under "Azure AD" or "Azure AD B2C" in Azure AD, which means you
must have an active Azure subscription.

To register your Drupal site as an app:
1. Open [the Azure portal](https://portal.azure.com/) and log-in.
2. Ensure the Portal has opened the correct subscription.
3. In the search box at the top of the page, enter "App Registration" and
   select the first result that appears.
4. Click the "New registration" button in the toolbar.
5. Enter a human-friendly name for your Drupal site in the "Name" text box.
6. Choose an access level appropriate for the users or applications your site
   is going to interact with under "Supported account types".
7. Leave the Redirect URI blank.
8. Un-check "Grant admin consent to openid and offline_access".
9. Click the "Register" button.
10. Wait for the application to be registered.
11. Copy the "Application (client) ID" value for later reference.
    This is the "Client ID" used by keys in this module.
12. Copy the "Directory (tenant) ID" value for later reference.
    This is the "Tenant ID" used by keys in this module.
13. Click on the "Certificates & secrets" link in the navigation on the left.
14. Click on the "New client secret" button in the toolbar at the bottom of the
    blade.
15. Enter a description that is meaningful to you in the "Description" text box.
16. Select an expiration date that meets your organization's security policies.
    A later expiration date is a lower maintenance burden but is not as secure
    since a leaked or stolen secret can be used for a longer period of time
    before it is rotated.
17. Click the "Add" button.
18. Click on the "API permissions" link in the navigation on the left.
19. Click on the "Add a permission" button in the toolbar inside the blade.
20. Click on the "Microsoft Graph" tile.
21. Click on the "Application permissions" tile.
22. Select the permission(s) appropriate for the kinds of activities that you
    will be performing through the Graph API from your Drupal site. Err on the
    side of selecting only the exact permissions you require; fewer permission
    are more secure than selecting many permissions since they limit exposure in
    the event that a secret is leaked.
23. Click the "Add permissions" button.
24. Click the "Grant admin consent for ..." button in the toolbar inside the
    blade.
25. Click the "Yes" button.

### Getting the Default Graph API Client
After installation, in order for there to be a default API client available,
you must either:
- Edit the "Default MS Graph API Key" under:
  "Configuration" -> "System" -> "Keys"
  (`/admin/config/system/keys/manage/ms_graph_api_default_key`);
  OR
- Define a new key of type "MS Graph API Key" under
  "Configuration" -> "System" -> "Keys", and then set the site to use the new
  key as the default API key under:
  "Configuration" -> "Web services" -> "Microsoft Graph API"
  (`/admin/config/services/ms-graph-api`).

Use the Tenant ID, Client ID, and Client Secret values you obtained when you
registered your app (described in the previous section).

Afterwards, you can obtain the default, authenticated API graph by getting the
`ms_graph_api.graph` service and then calling any of the methods on the object
you get back that are
[documented for the PHP library](https://github.com/microsoftgraph/msgraph-sdk-php#call-microsoft-graph-using-the-v10-endpoint-and-models).
For example, in procedural code and hooks:
```php
use Microsoft\Graph\Model;

$graph = \Drupal::service('ms_graph_api.graph');

$user =
  $graph
    ->createRequest("GET", "/me")
    ->setReturnType(Model\User::class)
    ->execute();

echo "Hello, I am {$user->getGivenName()}.";
```

### Getting a MS Graph API Graph for a Custom Key
If your site needs to work with multiple Azure subscriptions, you do not need
to configure the default key. Instead, define custom keys (of type
"MS Graph API Key") for each of the different deployments, obtain the Graph
API graph factory service, and construct a different API client for each custom
key.

For example:
```php
use Microsoft\Graph\Model;

$graph_factory = \Drupal::service('ms_graph_api.graph.factory');
$graph = $client_factory->buildGraphFromKeyId('my_graph_api_key');

$user =
  $graph
    ->createRequest("GET", "/me")
    ->setReturnType(Model\User::class)
    ->execute();

echo "Hello, I am {$user->getGivenName()}.";
```
